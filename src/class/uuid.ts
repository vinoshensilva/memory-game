class uuid {
    generate(): string {
        const randomNumber: number = Math.floor((Math.random() * 10000)) + 1;
        return `${randomNumber}${Date.now()}`;
    }
}

export default new uuid();