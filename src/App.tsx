// Import libraries
import { useDispatch, useSelector } from 'react-redux'

// Import stores
import { TStore } from './store'

// Import constants
import { ENDED, PRE_GAME } from './constants/game';

// Import components
import Cards from './components/cards/Cards';
import StartMenu from './components/menu/StartMenu';
import EndMenu from './components/menu/EndMenu';

// Import actions
import { start } from './slice/game';

function App() {

  const dispatch = useDispatch();

  const currentState = useSelector((state: TStore) => state.gameReducer.currentState);

  const startGameHandler = () => {
    dispatch(start());
  }

  return (
    <div className="h-screen w-full bg-gray-100">
      <div className='h-full w-full grid content-center gap-10'>
        <div className='text-center'>
          <h1 className='text-4xl font-bold text-indigo-500'>
            MEMORY GAME
          </h1>
        </div>
        {currentState === PRE_GAME && <StartMenu startGameHandler={startGameHandler} />}
        {currentState === ENDED && <EndMenu restartGameHandler={startGameHandler} />}
        {currentState !== PRE_GAME && <Cards />}
      </div>
    </div>
  )
}

export default App