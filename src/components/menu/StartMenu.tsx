// Import component
import Button from '../ui/Button';

// Declare interface
interface StartMenuProps {
    startGameHandler: () => void
}

const StartMenu: React.FC<StartMenuProps> = ({ startGameHandler }) => {
    return (
        <div className='mx-auto text-center'>
            <div>
                <Button onClick={startGameHandler}>
                    START GAME
                </Button>
            </div>
        </div>
    )
}

export default StartMenu