// Import component
import Button from '../ui/Button';

// Declare interface
interface StartMenuProps {
    restartGameHandler: () => void
}

const StartMenu: React.FC<StartMenuProps> = ({ restartGameHandler }) => {
    return (
        <div className='mx-auto text-center'>
            <p className='text-lg text-center text-indigo-500 font-bold'>
                YOU WON!!!
            </p>
            <div>
                <Button onClick={restartGameHandler}>
                    RESTART GAME
                </Button>
            </div>
        </div>
    )
}

export default StartMenu