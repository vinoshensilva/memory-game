import { TCardState } from '../../slice/game';

import { cardsData } from '../../data/cards';
import { MemoryIcon } from '../icons';

interface CardProps {
    card: TCardState,
    flipHandler: () => void
}

const Card: React.FC<CardProps> = ({ card, flipHandler }) => {

    const {
        flipped,
        id
    } = card;

    const Icon = cardsData[id];

    const classes = `${flipped ? ' flipped ' : ' '} bg-red-500 flip__card-inner w-14 h-14 sm:w-20 sm:h-20 cursor-pointer hover:border-0 hover:border-indigo-500`

    return (
        <div className="flip__card">
            <div onClick={flipHandler} className={classes}>
                <div className="flip__card-front grid content-center justify-center bg-white">
                    <MemoryIcon />
                </div>
                <div className="flip__card-back bg-indigo-500">
                    <Icon />
                </div>
            </div>
        </div >
    )
}

export default Card