// Import libraries
import { useDispatch, useSelector } from 'react-redux';

// Import functions
import { sleep } from '../../functions/async';

// Import slice
import { check, selectCard } from '../../slice/game';

// Import components
import Card from './Card';

// Import stores
import { TStore } from '../../store/index'
import { PLAYING } from '../../constants/game';

const Cards: React.FC = (): JSX.Element => {

    const cards = useSelector((state: TStore) => state.gameReducer.cards);
    const currentState = useSelector((state: TStore) => state.gameReducer.currentState);

    console.log(currentState);

    const dispatch = useDispatch();

    const flipHandler = async (cardIndex: number): Promise<void> => {

        if (currentState === PLAYING) {
            dispatch(selectCard({ cardIndex }));

            await sleep(600)

            dispatch(check())
        }
    }

    return (
        <div className='grid grid-rows-3 grid-flow-col gap-2 justify-center flex-wrap'>
            {cards.map((card, i) => (
                <div key={card.uniqueId}>
                    <Card
                        flipHandler={flipHandler.bind(null, i)}
                        card={card}
                    />
                </div>
            ))}
        </div>
    )
}

export default Cards