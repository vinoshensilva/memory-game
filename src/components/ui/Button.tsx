
interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
}

const Button: React.FC<ButtonProps> = ({ children, ...props }) => {
    return (
        <button
            className="bg-indigo-500 border-2 p-2 rounded-lg text-white hover:bg-indigo-600 hover:scale-105 hover:transition-all"
            {...props}
        >
            {children}
        </button>
    )
}

export default Button