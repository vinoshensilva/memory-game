export const CAMERA: string = 'CAMERA';
export const BRIEF: string = 'BRIEF';
export const CLOCK: string = 'CLOCK';
export const COG: string = 'COG';
export const EMOJI_HAPPY: string = 'EMOJI_HAPPY';
export const EMOJI_SAD: string = 'EMOJI_SAD';
export const EYE: string = 'EYE';
export const GLOBE: string = 'GLOBE';
export const HEART: string = 'HEART';
export const MOON: string = 'MOON';
export const MUSIC: string = 'MUSIC';
export const BUILDING: string = 'BUILDING';
export const AIRPLANE: string = 'AIRPLANE';
export const PENCIL: string = 'PENCIL';
export const SUN: string = 'SUN';
export const STAR: string = 'STAR';
export const TRUCK: string = 'TRUCK';
export const ACADEMIC: string = 'ACADEMIC';
export const BEAKER: string = 'BEAKER';
export const BELL: string = 'BELL';
export const CAKE: string = 'CAKE';
export const CHIP: string = 'CHIP';

import {
    AcademicIcon,
    AirplaneIcon,
    BeakerIcon,
    BellIcon,
    BriefCaseIcon,
    BuildingIcon,
    CakeIcon,
    CameraIcon,
    ChipIcon,
    ClockIcon,
    CogIcon,
    EmojiHappyIcon,
    EmojiSadIcon,
    EyeIcon,
    GlobeIcon,
    HeartIcon,
    MoonIcon,
    MusicIcon,
    PencilIcon,
    StarIcon,
    SunIcon,
    TruckIcon
} from '../components/icons/index'

export const cardsData: { [key: string]: Function } = {
    [CAMERA]: CameraIcon,
    [BRIEF]: BriefCaseIcon,
    [CLOCK]: ClockIcon,
    [COG]: CogIcon,
    [EMOJI_HAPPY]: EmojiHappyIcon,
    [EMOJI_SAD]: EmojiSadIcon,
    [EYE]: EyeIcon,
    [GLOBE]: GlobeIcon,
    [HEART]: HeartIcon,
    [MOON]: MoonIcon,
    [MUSIC]: MusicIcon,
    [BUILDING]: BuildingIcon,
    [AIRPLANE]: AirplaneIcon,
    [PENCIL]: PencilIcon,
    [SUN]: SunIcon,
    [STAR]: StarIcon,
    [TRUCK]: TruckIcon,
    [ACADEMIC]: AcademicIcon,
    [BEAKER]: BeakerIcon,
    [BELL]: BellIcon,
    [BEAKER]: BeakerIcon,
    [CAKE]: CakeIcon,
    [CHIP]: ChipIcon,
} 