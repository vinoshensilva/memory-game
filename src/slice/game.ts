import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import uuid from "../class/uuid";
import { ENDED, PLAYING, PRE_GAME, SELECTING } from "../constants/game";
import { cardsData } from "../data/cards";

// Define types
export interface TCardState {
    flipped: boolean,
    id: string // Key To The SVG Icons
    matched: boolean,
    uniqueId: string
}

export interface TGameState {
    currentState: string,
    firstCardIndex: number,
    secondCardIndex: number,
    cardsAmount: number,
    selectionCompleted: boolean,
    cards: TCardState[]
}

// Define payload types
type T = {
    id: string
}

type TSelectCardPayload = {
    cardIndex: number,
}

type TSelectCardsAmountPayload = {
    newCardsAmount: number
}

const initialState: TGameState = {
    currentState: PRE_GAME,
    firstCardIndex: -1,
    secondCardIndex: -1,
    selectionCompleted: false,
    cardsAmount: 6,
    cards: []
}

const gameSlice = createSlice({
    name: 'game',
    initialState,
    reducers: {
        reset: (state: TGameState) => {
            state = { ...initialState }
        },
        check: (state: TGameState) => {
            const {
                currentState,
                selectionCompleted,
                cards,
                firstCardIndex,
                secondCardIndex
            } = state;

            // If it is not selecting state currently
            // If selectionCompleted is not true
            if (currentState !== PLAYING) {
                return state;
            }

            if (!selectionCompleted) {
                return state;
            }

            const cardOneId = cards[firstCardIndex].id;
            const cardTwoId = cards[secondCardIndex].id;

            if (cardOneId === cardTwoId) {
                state.cards[firstCardIndex].matched = true;
                state.cards[secondCardIndex].matched = true;
            }
            else {
                state.cards[firstCardIndex].flipped = false;
                state.cards[secondCardIndex].flipped = false;
            }

            const hasGameEnded = cards.every((card) => card.matched);

            if (hasGameEnded) {
                state.currentState = ENDED;
            }

            state.firstCardIndex = -1;
            state.secondCardIndex = -1;
            state.selectionCompleted = false;
        },
        selectCard: (state: TGameState, { payload }: PayloadAction<TSelectCardPayload>) => {
            const { cardIndex } = payload;

            const card = state.cards[cardIndex];

            // If game is not in playing mode
            // If card at that particular index is valud
            // If card is already flipped
            // If selection is already complete
            if (state.currentState !== PLAYING) {
                return state;
            }

            if (!card) {
                return state;
            }

            if (card.flipped) {
                return state;
            }

            if (state.selectionCompleted) {
                return state;
            }

            if (state.firstCardIndex === -1) {
                state.firstCardIndex = cardIndex;
            }
            else if (state.secondCardIndex === -1) {
                state.secondCardIndex = cardIndex;
            }

            state.cards[cardIndex].flipped = true;

            state.selectionCompleted = (state.firstCardIndex !== -1 && state.secondCardIndex !== -1);
        },
        selectCardsAmount: (state: TGameState, { payload }: PayloadAction<TSelectCardsAmountPayload>) => {
            const { newCardsAmount } = payload;

            // If amount is not even
            // If amount less than 0
            // If amount exceeds the total prepared data
            if (newCardsAmount % 2 !== 0) {
                return state;
            }

            if (newCardsAmount < 0) {
                return state;
            }

            if (newCardsAmount > Object.keys(cardsData).length) {
                return state;
            }

            state.cardsAmount = newCardsAmount;
        },
        start: (state: TGameState) => {
            const { cardsAmount } = state;

            const newCards: Array<TCardState> = new Array(cardsAmount).fill(null);

            const keys = Object.keys(cardsData);

            const maxIterations = cardsAmount / 2;

            state.currentState = PLAYING;

            for (let i = 0; i < maxIterations; i++) {
                // Get a svg icon id from the cardsData
                const randomCardIndex: number = Math.floor(Math.random() * (keys.length));

                let numberOfPlacements = 0;

                do {
                    const randomPlacementIndex = Math.floor(Math.random() * (cardsAmount));

                    if (!newCards[randomPlacementIndex]) {
                        newCards[randomPlacementIndex] = {
                            uniqueId: uuid.generate(),
                            id: keys[i],
                            flipped: false,
                            matched: false
                        };

                        numberOfPlacements++;
                    }
                } while (numberOfPlacements < 2)

                // Remove the svg icon already used to prevent duplicates
                keys.splice(randomCardIndex, 1)
            }

            state.currentState = PLAYING;

            state.cards = newCards;
        }
    }
})

export const { reset, selectCard, selectCardsAmount, start, check } = gameSlice.actions;

export default gameSlice.reducer